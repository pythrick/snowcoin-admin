import {Component, OnInit} from '@angular/core';
import {TransactionService} from '../../services/transaction.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CurrencyPipe} from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  transactionForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  success = '';

  constructor(
    private transactionService: TransactionService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private cp: CurrencyPipe) {
  }

  ngOnInit() {
    this.transactionForm = this.formBuilder.group({
      checkingAccount: ['', Validators.required],
      amount: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.transactionForm.controls;
  }

  cobrar() {
    // stop here if form is invalid
    if (this.transactionForm.invalid) {
      return;
    }

    return this.fazerTransacao(this.f.checkingAccount.value, Math.abs(this.f.amount.value) * -1);

  }

  depositar() {
    // stop here if form is invalid
    if (this.transactionForm.invalid) {
      return;
    }

    return this.fazerTransacao(this.f.checkingAccount.value, Math.abs(this.f.amount.value));
  }

  fazerTransacao(checkingAccount, amount) {
    this.submitted = true;
    this.success = '';
    this.error = '';

    this.loading = true;
    this.transactionService.createTransaction(checkingAccount, amount)
      .subscribe(
        data => {
          const balance = this.cp.transform(data.balance, 'BRL', 'SC$');
          this.success = `Transação realizada com sucesso. Saldo atual: ${balance} .`;
          this.loading = false;
          this.transactionForm.reset();
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

}
