import {Injectable} from '@angular/core';

import {Transaction} from '../models/transaction.model';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) {
  }

  createTransaction(checkingAccount, amount) {
    return this.http.post<Transaction>(
      `${environment.apiUrl}/transactions/`,
      {checking_account: checkingAccount, amount});
  }
}
