import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private accessTokenSubject: BehaviorSubject<string>;
  public accessToken: Observable<string>;

  public get currentAccessToken() {
    return this.accessTokenSubject.value;
  }

  constructor(private http: HttpClient) {
    this.accessTokenSubject = new BehaviorSubject<string>(localStorage.getItem('accessToken'));
    this.accessToken = this.accessTokenSubject.asObservable();
  }

  public static getAccessToken(): string {
    return localStorage.getItem('accessToken');
  }

  login(email: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/auth/login/`, {email, password})
      .pipe(map(user => {
        localStorage.setItem('accessToken', user.access_token);
        this.accessTokenSubject.next(user.access_token);
        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('accessToken');
    this.accessTokenSubject.next(null);
  }
}
