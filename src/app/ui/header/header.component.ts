import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn = false;

  constructor(private router: Router, private authService: AuthService) {
    this.authService.accessToken.subscribe(x => {
      this.isLoggedIn = !!x;
    });
  }

  ngOnInit() {
  }

  logout() {
      this.authService.logout();
      this.router.navigate(['/login']);
  }

}
