import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ErrorInterceptor} from './helpers/error.interceptor';
import { HomeComponent } from './components/home/home.component';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {ReactiveFormsModule} from '@angular/forms';
import { LOCALE_ID } from '@angular/core';
import {CurrencyPipe, registerLocaleData} from '@angular/common';
import localePt from '@angular/common/locales/pt';
import {UiModule} from './ui/ui.module';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    UiModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    CurrencyPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
