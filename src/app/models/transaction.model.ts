import {JsonProperty} from 'json-object-mapper';

export class Transaction {
  @JsonProperty({name: 'checking_account'})
  checkingAccount: number;
  amount: number;
  uuid?: string;
  balance?: number;
  @JsonProperty({name: 'created_at'})
  createdAt?: string;
}
